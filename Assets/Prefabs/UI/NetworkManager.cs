﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager ins;

    [Header("LOGIN:")]
    public string login_URL;
    public TMP_InputField txtUserName_Login;
    public TMP_InputField txtPassword_Login;

    [Header("REGISTER:")]
    public string register_URL;
    public TMP_InputField txtUserName_Regis;
    public TMP_InputField txtFullName_Regis;
    public TMP_InputField txtPassword_Regis;

    public string token;
    private void Awake()
    {
        ins = this;   
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey(GameConstants.USER_NAME))
        {
            txtUserName_Login.text = PlayerPrefs.GetString(GameConstants.USER_NAME);
            txtPassword_Login.text = PlayerPrefs.GetString(GameConstants.PASSWORD);
        }
    }

    private void Update()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            UIMain.ins.ShowMess("No Internet");
        }
    }

    #region Login
    public void Login() {
        if (string.IsNullOrEmpty(txtUserName_Login.text) || string.IsNullOrEmpty(txtPassword_Login.text))
        {
            UIMain.ins.ShowMess("You need to fill in all information!");
        }
        else
        {
            StartCoroutine(LoginProcessing());
        }
    } 

    public IEnumerator LoginProcessing()
    {
        WWWForm form = new WWWForm();
        form.AddField("gameClient", "qlWwcACyJDv5wZFHHyFoHcZmiJ6VPSOO");
        form.AddField("username", txtUserName_Login.text);
        string hashedPassword = MD5Hasher.MD5Hash(txtPassword_Login.text);
        form.AddField("password", hashedPassword);
        UnityWebRequest rq = UnityWebRequest.Post(login_URL, form);

        yield return rq.SendWebRequest();
        if (rq.result == UnityWebRequest.Result.Success)
        {
            string responseText = rq.downloadHandler.text;
            ServerResponse serverResponse = JsonUtility.FromJson<ServerResponse>(responseText);

            if (serverResponse != null)
            {
                if (serverResponse.code == 404)
                {
                    UIMain.ins.ShowMess("Account or password is incorrect!");
                }
                else
                {
                    PlayerPrefs.SetString(GameConstants.USER_NAME,txtUserName_Login.text);
                    PlayerPrefs.SetString(GameConstants.PASSWORD,txtPassword_Login.text);
                    UIMain.ins.ShowRoom();
                }
            }
            else
            {
                UIMain.ins.ShowMess("Invalid JSON response!");
            }
        }
        else
        {
            UIMain.ins.ShowMess("NOT RESPONSE!");
        }
        rq.Dispose();   
    }
    #endregion

    #region Register
    public void Register() {
        if (string.IsNullOrEmpty(txtUserName_Regis.text) || string.IsNullOrEmpty(txtPassword_Regis.text) || string.IsNullOrEmpty(txtFullName_Regis.text))
        {
            UIMain.ins.ShowMess("You need to fill in all information!");
        }
        else
        {
            StartCoroutine(RegisterProcessing()); 
        }
    }  

    public IEnumerator RegisterProcessing()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", txtUserName_Regis.text);
        string hashedPassword = MD5Hasher.MD5Hash(txtPassword_Regis.text);
        form.AddField("password", hashedPassword);
        form.AddField("fullName", txtFullName_Regis.text);
        UnityWebRequest rq = UnityWebRequest.Post(register_URL, form);  
        //form.headers.Add("x-access-token", token);

        yield return rq.SendWebRequest();

        if (rq.result == UnityWebRequest.Result.Success)
        {
            string responseText = rq.downloadHandler.text;
            ServerResponse serverResponse = JsonUtility.FromJson<ServerResponse>(responseText);

            if (serverResponse != null)
            {
                if (serverResponse.code == 404)
                {
                    UIMain.ins.ShowMess("Account already existed!");
                }
                else
                {
                    UIMain.ins.ShowMess("SUCCESSFUL!");
                }
            }
            else
            {
                UIMain.ins.ShowMess("Invalid JSON response!");
            }
        }
        else
        {
            UIMain.ins.ShowMess("NOT RESPONSE!");
        }
        rq.Dispose();
    }
    #endregion  
}

[System.Serializable]
public class ServerResponse
{
    public int code;
    public string message;
    public LoginResponeData data;
}

public class LoginResponeData
{
    public UserDetail user;
    public string token;
}

public class UserDetail
{
    public string ID { get; set; }
    public string accountID { get; set; }
    public string fullName { get; set; }
    public string tiktokID { get; set; }
    public string sessionID { get; set; }
    public string createdAt { get; set; }
    public string updatedAt { get; set; }
}

