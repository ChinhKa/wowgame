using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tower : MonoBehaviour
{
    public IDTower id;
    public SliderBar healthBar;
    public List<Character> enemies = new List<Character>();

    private void Update()
    {
        //CheckEnemyList();
    }

    public void TakeDame(Character enemy, int damage)
    {
        Character e = enemies.Find(e => e.GetIDCharacter() == enemy.GetIDCharacter());
        if(enemy == null)
        {
            enemies.Add(enemy);
        }

        healthBar.ReduceValue(damage);

        if(healthBar.GetSliderValue() <= 0)
        {
            SoundManager.ins.PlayEffectSound(SoundManager.ins.explodeSound);
            GameManager.ins.listTowers.Remove(this);
            gameObject.SetActive(false);
            GameManager.ins.CheckWin();
        }
    }

    public void CheckEnemyList()
    {
        foreach (var e in enemies)
        {
            if(e.GetHealth() <= 0)
            {
                enemies.Remove(e);
            }
        }
    }

    public void SetMaxHealth()  
    {
        gameObject.SetActive(true);
        healthBar.SetMax(DataManager.ins.data.towerHealth, DataManager.ins.data.towerHealth);
    }
}
