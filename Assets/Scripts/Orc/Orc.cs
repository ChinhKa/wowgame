using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Orc : Character
{
    private void Start()
    {
        gameObject.layer = (int)LayerM.Orc;
        gameObject.tag = Tag.Orc.ToString();
        healthBar.transform.GetChild(0).GetComponent<Image>().color = Color.red;
    }

    public override void FindTower()
    {
        FindShortestDistance(GameManager.ins.targetTowers_Orc);
    }
}
