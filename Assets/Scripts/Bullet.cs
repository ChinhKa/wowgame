using UnityEngine;
using DG.Tweening;

public class Bullet : MonoBehaviour
{
    [Header("SPEED:")]
    [SerializeField] private float duration;

    [Space]
    [Header("EFFECT:")]
    [SerializeField] private ParticleSystem hitEff_Pre;
    private ParticleSystem hitEff;

    private void Start()
    {
        if (hitEff_Pre != null)
        {
            hitEff = Instantiate(hitEff_Pre);
            GameManager.ins.pool.Add(hitEff.gameObject);
        }
        GameManager.ins.pool.Add(gameObject);
    }

    public void SetTarget(Character sender, Transform target, int dame)
    {
        Vector3 lock_Target = target.position + new Vector3(0,2,0);
 
        #region Move
        transform.DOMove(lock_Target, duration).OnComplete(() =>
        {   
            if (transform.position == lock_Target)
            {
                if (hitEff != null)
                {
                    hitEff.transform.position = transform.position;
                    hitEff.Play();
                }
                if (target != null)
                {
                    if (target.GetComponent<Character>() != null)
                    {
                        target.GetComponent<Character>().TakeDame(sender,dame);                                        
                    }
                    else {
                        target.GetComponent<Tower>().TakeDame(sender,dame);                                        
                    }
                }
                gameObject.SetActive(false);
            }
            else
            {
                if (target == null)
                {
                    gameObject.SetActive(false);
                }
            }
        });
        #endregion
    }
}
