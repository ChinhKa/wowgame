using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderBar : MonoBehaviour
{
    [SerializeField] private Slider slider;

    public void SetMax(int maxValue, int value)
    {
        slider.maxValue = maxValue;
        slider.value = value;
    }

    public void IncreaseValue(int value) => slider.value += value;
    public void ReduceValue(int value) => slider.value -= value;

    public float GetSliderValue() => slider.value;
    public float GetSliderMaxValue() => slider.maxValue;
}
