using System.Collections;
using System.Collections.Generic;
using TikTokLiveSharp.Client;
using TikTokLiveSharp.Events;
using TikTokLiveUnity;
using UnityEngine;

public class TikTokController : MonoBehaviour
{
    public static TikTokController instance;
    private Dictionary<int, Dictionary<string, int>> listUser;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        listUser = new Dictionary<int, Dictionary<string, int>>();
        listUser.Add(1, new Dictionary<string, int>());
        listUser.Add(2, new Dictionary<string, int>());
        listUser.Add(3, new Dictionary<string, int>());
        listUser.Add(4, new Dictionary<string, int>());

        TikTokLiveManager.Instance.OnChatMessage += OnChatMessageReceive;
        TikTokLiveManager.Instance.OnGiftMessage += OnGiftMessageReceive;
    }

    public void OnGiftMessageReceive(TikTokLiveClient sender, GiftMessage gift)
    {
        Debug.LogError(gift.User.UniqueId + "-Cost:" + gift.Gift.DiamondCost + "-Amount:" + gift.Amount + "- GiftName:" + gift.Gift.Name);

        if (GameManager.ins.isReadySession)
        {
            #region test
            if (!listUser[1].ContainsKey(gift.User.UniqueId) &&
                  !listUser[2].ContainsKey(gift.User.UniqueId) && !listUser[3].ContainsKey(gift.User.UniqueId) &&
                  !listUser[4].ContainsKey(gift.User.UniqueId))
            {
                int rand = Random.Range(1, 5);
                listUser[rand].Add(gift.User.UniqueId, 0);

                switch (rand)
                {
                    case 1:
                        SpawnSoldier(gift.User.UniqueId, gift.User.NickName,IDCharacter.Human, GameManager.ins.humanTower.transform);
                        break;
                    case 2:
                        SpawnSoldier(gift.User.UniqueId, gift.User.NickName, IDCharacter.Elf, GameManager.ins.elfTower.transform);
                        break;
                    case 3:
                        SpawnSoldier(gift.User.UniqueId, gift.User.NickName, IDCharacter.Orc, GameManager.ins.orcTower.transform);
                        break;
                    case 4:
                        SpawnSoldier(gift.User.UniqueId, gift.User.NickName, IDCharacter.Undead, GameManager.ins.undeadTower.transform);
                        break;
                    default:
                        break;
                }
            }
            #endregion

            Character character = GameManager.ins.listSoldier.Find(c => c.GetUserID() == gift.User.UniqueId);
            if (character != null)
            {
                if (character.GetLevel() != Level.Level5 && character.GetHealth() > 0)
                {
                    int scores = (gift.Gift.DiamondCost * (int)gift.Amount);
                    character.IncreaseExp(scores);
                }
            }
        }
    }

    public void OnChatMessageReceive(TikTokLiveClient sender, Chat chat)
    {
        if (GameManager.ins.isReadySession)
        {
            if (!listUser[1].ContainsKey(chat.Sender.UniqueId) &&
                   !listUser[2].ContainsKey(chat.Sender.UniqueId) && !listUser[3].ContainsKey(chat.Sender.UniqueId) &&
                   !listUser[4].ContainsKey(chat.Sender.UniqueId))
            {
                switch (chat.Message)
                {
                    case "1":
                        listUser[1].Add(chat.Sender.UniqueId, 0);
                        SpawnSoldier(chat.Sender.UniqueId, chat.Sender.NickName ,IDCharacter.Human, GameManager.ins.humanTower.transform);
                        break;
                    case "2":
                        SpawnSoldier(chat.Sender.UniqueId, chat.Sender.NickName, IDCharacter.Elf, GameManager.ins.elfTower.transform);
                        listUser[2].Add(chat.Sender.UniqueId, 0);
                        break;
                    case "3":
                        SpawnSoldier(chat.Sender.UniqueId, chat.Sender.NickName, IDCharacter.Orc, GameManager.ins.orcTower.transform);
                        listUser[3].Add(chat.Sender.UniqueId, 0);
                        break;
                    case "4":
                        SpawnSoldier(chat.Sender.UniqueId, chat.Sender.NickName, IDCharacter.Undead, GameManager.ins.undeadTower.transform);
                        listUser[4].Add(chat.Sender.UniqueId, 0);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void SpawnSoldier(string userID, string name, IDCharacter iDCharacter, Transform tower)
    {
        /*  if (tower.GetComponent<Tower>().healthBar.GetSliderValue() > 0)
          {*/
        CharacterInfo info = DataManager.ins.data.characterDB.listCharacter.Find(c => c.level == Level.Level1 && c.ID == iDCharacter);
        int random = Random.Range(0, GameManager.ins.listTowers.Count);

        int idxRandomPos = Random.Range(1,5);
        Vector3 posRandom = Vector3.zero;
        switch (idxRandomPos)
        {
            case 1:
                posRandom = new Vector3(3,0,0);
                break;
            case 2:
                posRandom = new Vector3(-3,0,0);
                break;
            case 3:
                posRandom = new Vector3(0,0,3);
                break;
            case 4:
                posRandom = new Vector3(0, 0, -3);
                break;
        }

        Transform pre = Instantiate(info.prefab, GameManager.ins.listTowers[random].transform.position + posRandom, Quaternion.identity);
        pre.GetComponent<Character>().SetData(userID, name ,iDCharacter, info.type, info.attackSound, 0);
        GameManager.ins.listSoldier.Add(pre.GetComponent<Character>());
        UIManager.ins.UpdateSoldier(iDCharacter);
        //}
    }


    public void ResetUser()
    {
        listUser[1].Clear();
        listUser[2].Clear();
        listUser[3].Clear();
        listUser[4].Clear();
    }

    public void IncreaseEXP()
    {
        List<Character> list = new List<Character>();
        foreach (var o in GameManager.ins.listSoldier)
        {
            list.Add(o);
        }

        foreach (var o in list)
        {
            if (o != null && o.GetHealth() > 0)
            {
                o.IncreaseExp(500);
            }
        }
    }
}
