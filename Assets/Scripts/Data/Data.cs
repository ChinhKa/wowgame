using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "New Data")]
public class Data : ScriptableObject
{
    [Header("HARD DATA:")]
    public int towerHealth;
    public int timeRevival;
    public List<int> costUpgrade = new List<int>();

    [Header("TIME LINE:")]
    public float sessionTime;
    public float nextSessionTime;
    public float nextDaySessionTime;

    [Space]
    [Header("SOFT DATA:")]
    public CharacterDB characterDB;
}
