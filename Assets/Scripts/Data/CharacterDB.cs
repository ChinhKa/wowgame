﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DBCharacter", menuName = "New DB Character")]
public class CharacterDB : ScriptableObject
{
    public List<CharacterInfo> listCharacter = new List<CharacterInfo>();
    [Header("LEVEL:")]
    public List<LevelInfo> listLevel = new List<LevelInfo>();
    [Header("LEVEL BONUS:")]
    public LevelInfo levelBonus = new LevelInfo();
    [Header("RANGE:")]
    public List<Range> ranges = new List<Range>();
}

[System.Serializable]
public class CharacterInfo
{
    [Header("BASE:")]
    public IDCharacter ID;
    public Level level;
    public SoldierType type;
    public Transform prefab;
    public AudioClip attackSound;
}

[System.Serializable]
public class LevelInfo
{
    public Level level;
    public int HP;
    public int ATK;
    public float ATKS;
    public float moveSpeed;
   // public int attackRange;
}

[System.Serializable]
public class Range
{
    public SoldierType type;
    public int attackRange; // Phạm vi tấn công
    public float attackDistance; // Khoảng cách tấn công
}