using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightUndead : Undead
{
    public Animator petAnimator;

    public override void Update()
    {
        base.Update();
        if (HP > 0)
        {
            if (!agent.IsStopped)
            {
                petAnimator.SetTrigger("Run");
                Debug.Log("Running");
            }
            else
            {
                petAnimator.SetTrigger("Idle");
            }
        }
    }

    public override void TakeDame(Character sender, int damage)
    {
        base.TakeDame(sender, damage);
        if (HP <= 0)
        {
            petAnimator.SetTrigger("Die");
        }
    }

    public override void ResetData()
    {
        base.ResetData();
        petAnimator.Rebind();
    }
}
