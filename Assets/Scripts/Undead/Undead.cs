using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Undead : Character
{
    private void Start()
    {
        gameObject.layer = (int)LayerM.Undead;
        gameObject.tag = Tag.Undead.ToString();
        healthBar.transform.GetChild(0).GetComponent<Image>().color = Color.gray;
    }

    public override void FindTower()
    {
        FindShortestDistance(GameManager.ins.targetTowers_Undead);
    }
}
