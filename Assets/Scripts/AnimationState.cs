using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimationState
{
    public abstract void PlayAnimation();
}


public class IdleState : AnimationState
{
    public override void PlayAnimation()
    {
        
    }
}

public class RunState : AnimationState
{
    public override void PlayAnimation()
    {

    }
}

public class DieState : AnimationState
{
    public override void PlayAnimation()
    {

    }
}

public class HurtState : AnimationState
{
    public override void PlayAnimation()
    {

    }
}

public class AttackState : AnimationState
{
    public override void PlayAnimation()
    {

    }
}

public class WinState : AnimationState
{
    public override void PlayAnimation()
    {

    }
}