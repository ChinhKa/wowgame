using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEvent : MonoBehaviour
{
    public Character character;

    public void AttackProcessing() => character.AttackProcessing();
    public void EnableAllAttack() => character.AttackProcessing();
    public void Destroy() => Destroy(character.gameObject);
    public void EnableMove() => character.EnableMove();
}
