﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class GameManager : MonoBehaviour
{
    public static GameManager ins;

    [Header("EVENT:")]
    public bool isReadySession;
    public bool finishSession;
    public bool finishDaySession;

    public Action OnFinishSeesion;

    public List<Transform> targetTowers_Elf = new List<Transform>();
    public List<Transform> targetTowers_Human = new List<Transform>();
    public List<Transform> targetTowers_Orc = new List<Transform>();
    public List<Transform> targetTowers_Undead = new List<Transform>();


    [HideInInspector] public List<Tower> listTowers = new List<Tower>();

    public Tower elfTower;
    public Tower humanTower;
    public Tower orcTower;
    public Tower undeadTower;

    public List<Character> listSoldier = new List<Character>();

    public List<GameObject> pool = new List<GameObject>();

    [Space]
    [Header("COOL DOWN:")]
    private float countDown_SessionTime;
    [SerializeField] private float countDown_NextSessionTime;
    private float countDown_NextDaySessionTime;

    private DateTime currentDateTime;
    private DateTime endOfDay;

    private void Start()
    {
        ins = this;
        OnFinishSeesion += HandleFinishedSeesion;
        ResetSession();
    }

    private void Update()
    {
        if (!finishSession)
        {
            if (isReadySession)
            {
                SessionTime();
            }
        }
        else
        {
            NextSessionTime();
        }
    }

    public void SetData()
    {
        countDown_SessionTime = DataManager.ins.data.sessionTime;
        countDown_NextSessionTime = DataManager.ins.data.nextSessionTime;
        finishSession = false;
    }

    public void CheckWin()
    {
        if (listTowers.Count == 1 || finishSession)
        {
            isReadySession = false;
            if (finishSession)
            {
                UIManager.ins.ShowResult("NO WINNER!");
            }
            else
            {
                switch (listTowers[0].id)
                {
                    case IDTower.OrcTower:
                        UIManager.ins.ShowResult("Orc Win");
                        break;
                    case IDTower.HumanTower:
                        UIManager.ins.ShowResult("Human Win");
                        break;
                    case IDTower.UndeadTower:
                        UIManager.ins.ShowResult("Undead Win");
                        break;
                    case IDTower.ElfTower:
                        UIManager.ins.ShowResult("Elf Win");
                        break;
                }
            }
        }
    }

    private void SessionTime()
    {
        if (countDown_SessionTime <= 0)
        {
            UIManager.ins.txtSessionTime.text = "00:00";
            OnFinishSeesion?.Invoke();
        }
        else
        {
            countDown_SessionTime -= Time.deltaTime;
            UIManager.ins.txtSessionTime.text = Mathf.Floor(countDown_SessionTime / 60).ToString("00") + ":" + (countDown_SessionTime % 60).ToString("00");
        }
    }

    private void NextSessionTime()
    {
        countDown_NextSessionTime -= Time.deltaTime;
        if (countDown_NextSessionTime <= 0)
        {
            SetData();
            ResetSession();
        }
        else
        {
            UIManager.ins.txtNextSessionTime.text = Math.Round(countDown_NextSessionTime).ToString();
        }
    }

    private void ResetSession()
    {
        foreach(var o in listSoldier)
        {
            if (o != null)
            {
                Destroy(o.gameObject);
            }
        }

        foreach(var p in pool)
        {
            Destroy(p);
        }
        pool.Clear();
        listSoldier.Clear();
        if(TikTokController.instance != null)
        {
            TikTokController.instance.ResetUser();
        }
        ResetTower();
        SetData();
        UIManager.ins.Active(UIManager.ins.fightUI);
        UIManager.ins.ShowTimeEffect();
    }

    private void HandleFinishedSeesion()
    {
        SoundManager.ins.PlayEffectSound(SoundManager.ins.finishSound);
        finishSession = true;
        CheckWin();
    }
   
    private void ResetTower()
    {
        elfTower.SetMaxHealth();
        humanTower.SetMaxHealth();
        undeadTower.SetMaxHealth();
        orcTower.SetMaxHealth();
        listTowers.Clear();

        listTowers.Add(elfTower);
        listTowers.Add(humanTower);
        listTowers.Add(undeadTower);
        listTowers.Add(orcTower);
    }
}
