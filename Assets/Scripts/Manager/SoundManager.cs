using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager ins;

    [Header("AUDIO SOURCE:")]
    public AudioSource musicSound_AudioSource;
    public AudioSource effectSound_AudioSource;

    [Space]
    [Header("AUDIO CLIP:")]
    public AudioClip BGClip;
    public AudioClip countDownFightClip;
    public AudioClip finishSound;
    public AudioClip explodeSound;

    private float countDownClap;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        PlayMusicSound(true);
    }

    public void PlayMusicSound(bool isLoop)
    {
        musicSound_AudioSource.clip = BGClip;
        musicSound_AudioSource.Play();
        musicSound_AudioSource.loop = isLoop;
    }

    public void PlayEffectSound(AudioClip clip)
    {
        effectSound_AudioSource.PlayOneShot(clip);
    }
}
