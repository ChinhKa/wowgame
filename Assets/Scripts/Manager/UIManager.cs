using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections.Generic;
using System.Collections;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;

    [Header("UI:")]
    public GameObject resultUI;
    public GameObject finishDaySession_UI;
    public GameObject fightUI;

    [Space]
    [Header("TEXT:")]
    public TextMeshProUGUI txtResult;
    public TextMeshProUGUI nbrsElf;
    public TextMeshProUGUI nbrsHuman;
    public TextMeshProUGUI nbrsOrc;
    public TextMeshProUGUI nbrsUndead;
    public TextMeshProUGUI txtNextSessionTime;
    public TextMeshProUGUI txtSummaryTime;
    public TextMeshProUGUI txtSessionTime;
    public TextMeshProUGUI txtShowTime;
    public TextMeshProUGUI txtNextDaySessionTime;

    [Space]
    [Header("BUTTON:")]
    public Button btnUpgrade_Cheat;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        Active(fightUI);
        ResetSoldier();

        btnUpgrade_Cheat.onClick.AddListener(() =>
        {
            if (GameManager.ins.listSoldier.Count != 0)
            {
                TikTokController.instance.IncreaseEXP();
            }
        });
    }

    public void Active(GameObject ui)
    {
        fightUI.SetActive(false);
        resultUI.SetActive(false);
        finishDaySession_UI.SetActive(false);
        if (ui != null)
        {
            ui.SetActive(true);
        }
    }

    public void ShowResult(string winner)
    {       
        txtResult.text = winner;         
        GameManager.ins.finishSession = true;
        Active(resultUI);
    }

    public void ResetSoldier()
    {
        nbrsElf.text = "SL: 0";
        nbrsHuman.text = "SL: 0";
        nbrsOrc.text = "SL: 0";
        nbrsUndead.text = "SL: 0";
    }

    public void UpdateSoldier(IDCharacter iD)
    {
        List<Character> list = GameManager.ins.listSoldier.FindAll(c => c.GetIDCharacter() == iD);
        switch (iD)
        {
            case IDCharacter.Human:
                nbrsHuman.text = "SL: " + list.Count;
                break;
            case IDCharacter.Elf:
                nbrsElf.text = "SL: " + list.Count;
                break;
            case IDCharacter.Orc:
                nbrsOrc.text = "SL: " + list.Count;
                break;
            case IDCharacter.Undead:
                nbrsUndead.text = "SL: " + list.Count;
                break;
            default:
                break;
        }
    }

    public void ShowTimeEffect() => StartCoroutine(ShowTimeEffect_Processing()); 

    private IEnumerator ShowTimeEffect_Processing()
    {
        txtShowTime.gameObject.SetActive(true);
        txtShowTime.transform.localScale = Vector3.zero;
        txtShowTime.transform.DOScale(Vector3.one,0.5f).SetDelay(0.5f);
        yield return new WaitForSeconds(4);
        txtShowTime.gameObject.SetActive(false);
        GameManager.ins.isReadySession = true;
    }
}
