public class GameConstants
{
    public const string USER_NAME = "USER_NAME";
    public const string PASSWORD = "PASSWORD";
}

public enum StateAnimation
{
    Idle,
    Attack,
    Run,
    Win,
    Hurt,
    Die
}

public enum IDCharacter
{
    Human = 1,
    Elf = 2,
    Orc = 3,
    Undead = 4
}

public enum Level
{
    Level1 = 0,
    Level2 = 1,
    Level3 = 2,
    Level4 = 3,
    Level5 = 4,
    levelBonus
}

public enum SoldierType
{
    melee = 0,
    ranged = 1
}

public enum Tag
{
    Human = 0,
    Orc = 1,
    HumanTower = 2,
    OrcTower = 3,
    UndeadTower = 4,
    ElfTower = 5,
    Elf = 6,
    Undead = 7,
    ManKind = 8,
    Ground = 9
}

public enum LayerM
{
    Human = 6,
    Orc = 7,
    Undead = 8,
    Elf = 9
}

public enum Key
{
    RoomID
}

public enum IDTower
{
    OrcTower = 0,
    HumanTower = 1,
    UndeadTower = 2,
    ElfTower = 3
}