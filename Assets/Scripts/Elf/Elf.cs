using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Elf : Character
{
    private void Start()
    {
        gameObject.layer = (int)LayerM.Elf;
        gameObject.tag = Tag.Elf.ToString();
        healthBar.transform.GetChild(0).GetComponent<Image>().color = Color.green;
    }

    public override void FindTower()
    {
        FindShortestDistance(GameManager.ins.targetTowers_Elf);
    }

 /*   public override IEnumerator Revival()
    {
        yield return new WaitForSeconds(DataManager.ins.data.timeRevival);
        if (GameManager.ins.elfTower.healthBar.GetSliderValue() > 0)
        {
            transform.position = GameManager.ins.elfTower.transform.position;
            ResetData();
        }
        else
        {
            Destroy(gameObject); 
        }
    }*/
}
