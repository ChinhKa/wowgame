﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using ProjectDawn.LocalAvoidance;
using TMPro;

public class Character : MonoBehaviour
{
    [Header("REFERANCE:")]
    [SerializeField] protected Animator animator;
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] protected IDCharacter ID;
    protected Level level;
    protected string userID;

    [Space]
    [Header("BASE:")]
    protected int HPBase;
    protected int HP;
    protected int ATK;
    protected float ATKS;
    protected float moveSpeed;
    protected int attackRange;
    protected AudioClip attackSound;

    [Space]
    [Header("SLIDER:")]
    [SerializeField] protected SliderBar healthBar;
    [SerializeField] protected SliderBar expBar;
    [SerializeField] protected TextMeshProUGUI txtName;

    [Space]
    [Header("OTHER:")]
    protected Collider[] listE;
    protected Transform target;
    protected float countDownATK;
    protected bool enableMove;
    protected bool enableHurt;

    [SerializeField] protected Agent agent;
    [SerializeField] protected FollowAgent followAgent;

    public Character sender;

    public virtual void Update()
    {
        if (!GameManager.ins.finishSession)
        {
            FindTarget();
            if (target != null)
            {
                if (target.GetComponent<Character>() != null && target.GetComponent<Character>().HP <= 0 ||
                  target.GetComponent<Tower>() != null && target.GetComponent<Tower>().healthBar.GetSliderValue() <= 0
                  )
                {
                    target = null;
                }
                if (enableMove)
                {
                    Move();
                }
            }
        }
        else
        {
            if (HP < 0)
            {
                Destroy(gameObject);
            }
            else
            {
                animator.SetTrigger(StateAnimation.Idle.ToString());
                agent.emergencyStop = true;
            }
        }
    }

    private void FindTarget()
    {
        listE = Physics.OverlapSphere(transform.position, attackRange);
        if (listE.Length > 0)
        {
            List<Transform> list = new List<Transform>();
            foreach (var o in listE)
            {
                if (o.GetComponent<Character>() != null && o.GetComponent<Character>().ID != ID)
                {
                    list.Add(o.transform);
                }
            }
            if (list.Count != 0)
            {
                FindShortestDistance(list);
            }
            else
            {
                FindTower();
            }
        }
    }

    public virtual void FindTower()
    {

    }

    public virtual void ResetData()
    {
        animator.Rebind();
        GetComponent<CapsuleCollider>().enabled = true;
        this.enabled = true;
        enableMove = true;
        agent.emergencyStop = false;
        HP = HPBase;
        healthBar.SetMax(HPBase, HPBase);
        GetComponent<CapsuleCollider>().enabled = true;
        healthBar.transform.parent.gameObject.SetActive(true);
    }

    public virtual void FindShortestDistance(List<Transform> listFind)
    {
        if (listFind.Count > 0)
        {
            foreach (var o in listFind)
            {
                if (!o.gameObject.activeInHierarchy)
                {
                    continue;
                }
                else
                {
                    if (target == null)
                    {
                        target = o.transform;
                    }
                }
                if (Vector3.Distance(transform.position, o.transform.position) < Vector3.Distance(transform.position, target.transform.position))
                {
                    target = o.transform;
                }
            }
            followAgent.TargetAgent = target.GetComponent<Agent>();
        }
    }

    private void Move()
    {
        if (agent.IsStopped)
        {
            LookatTarget();
            //agent.emergencyStop = true;
            animator.SetTrigger(StateAnimation.Idle.ToString());
            if (countDownATK >= ATKS)
            {
                enableHurt = false;
                Attack();
                countDownATK = 0;
            }
            else
            {
                if (enableMove)
                {
                    enableHurt = true;
                }
                countDownATK += Time.deltaTime;
            }
        }
        else
        {
            //agent.emergencyStop = false;
            //transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            animator.SetTrigger(StateAnimation.Run.ToString());
        }
    }

    private void LookatTarget()
    {
        if (target != null)
        {
            Vector3 direction = target.position - transform.position;
            direction.y = 0;
            Quaternion rotation = Quaternion.LookRotation(direction);
            float rotationSpeed = 5.0f; // Tốc độ quay
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
        }
    }

    private void Attack()
    {
        LookatTarget();
        // GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        animator.SetTrigger(StateAnimation.Attack.ToString());
        enableMove = false;
    }

    public void EnableMove() => enableMove = true;
    public IDCharacter GetIDCharacter() => ID;
    public string GetUserID() => userID;
    public Level GetLevel() => level;
    public int GetHealth() => HP;

    public virtual void AttackProcessing()
    {
        if (target != null)
        {
            PlaySound(attackSound);
            if (target.GetComponent<Character>() != null)
            {
                target.GetComponent<Character>().TakeDame(this,ATK);
            }
            else
            {
                target.GetComponent<Tower>().TakeDame(this,ATK);
            }
        }
    }

    public virtual void TakeDame(Character sender, int damage)
    {
        if (HP > 0)
        {
            HP -= damage;

            ChaseSender();

            if (enableHurt)
            {
                animator.SetTrigger(StateAnimation.Hurt.ToString());
            }

            healthBar.ReduceValue(damage);
        }

        if (HP <= 0)
        {
            Die();
        }
    }

    public virtual void ChaseSender()
    {
        if (sender != null && target.GetComponent<Tower>() != null)
        {
            Debug.Log("cap may tinh");
            target = sender.transform;
        }
    }

    private void Die()
    {
        UIManager.ins.UpdateSoldier(ID);
        healthBar.transform.parent.gameObject.SetActive(false);
        animator.SetTrigger(StateAnimation.Die.ToString());
        StartCoroutine(Revival());
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        // GameManager.ins.listSoldier.Remove(this);
        agent.emergencyStop = true;
        StartCoroutine(Hidden());
        this.enabled = false;
    }

    private IEnumerator Hidden()
    {
        yield return new WaitForSeconds(2);
        transform.position = new Vector3(transform.position.x, -20, transform.position.z);
    }

    public virtual IEnumerator Revival()
    {
        yield return new WaitForSeconds(DataManager.ins.data.timeRevival);
        int random = Random.Range(0, GameManager.ins.listTowers.Count);
        transform.position = GameManager.ins.listTowers[random].transform.position + new Vector3(0, 0, -2);
        ResetData();
    }

    public void Upgrade(/*int redundantExp*/)
    {
        int nextLevel = (int)level + 1;
        CharacterInfo info = DataManager.ins.data.characterDB.listCharacter.Find(c => c.ID == ID && (int)c.level == nextLevel);
        if (info != null)
        {
            Transform pre = Instantiate(info.prefab, transform.position, transform.rotation);
            pre.GetComponent<Character>().SetData(userID, txtName.text, ID, info.type, info.attackSound, nextLevel);
            //  pre.GetComponent<Character>().IncreaseExp(redundantExp);
            GameManager.ins.listSoldier.Remove(this);
            GameManager.ins.listSoldier.Add(pre.GetComponent<Character>());
            Destroy(gameObject);
        }
    }

    public void SetData(string userId, string name, IDCharacter id, SoldierType type, AudioClip attackSound, int lv)
    {
        ID = id;
        LevelInfo info = DataManager.ins.data.characterDB.listLevel.Find(l => (int)l.level == lv);
        level = info.level;

        HPBase = HP = info.HP;
        ATK = info.ATK;
        ATKS = info.ATKS;
        moveSpeed = info.moveSpeed;
        agent.Speed = info.moveSpeed;
        this.attackSound = attackSound;
        Range range = DataManager.ins.data.characterDB.ranges.Find(r => r.type == type);
        agent.StopDistance = range.attackDistance;
        attackRange = range.attackRange;
        userID = userId;
        txtName.text = name;
        enableMove = true;
        healthBar.SetMax(HPBase, HPBase);
        if (DataManager.ins.data.costUpgrade.Count - 1 <= lv)
        {
            expBar.SetMax(1, 1);
        }
        else
        {
            expBar.SetMax(DataManager.ins.data.costUpgrade[lv + 1], 0);
        }
    }

    public void IncreaseExp(int value)
    {
        int oldValues = (int)expBar.GetSliderValue();
        expBar.IncreaseValue(value);
        if (expBar.GetSliderValue() >= expBar.GetSliderMaxValue())
        {
            Upgrade(/*(oldValues + value) - (int)expBar.GetSliderMaxValue()*/);
            Debug.Log("Upgraded!");
        }
    }

    public virtual void PlaySound(AudioClip clip)
    {
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(clip);
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
