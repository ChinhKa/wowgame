using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class Human : Character
{
    private void Start()
    {
        gameObject.layer = (int)LayerM.Human;
        gameObject.tag = Tag.Human.ToString();
        healthBar.transform.GetChild(0).GetComponent<Image>().color = Color.yellow;
    }

    public override void FindTower()
    {
        FindShortestDistance(GameManager.ins.targetTowers_Human);
    }
}
