using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanADC : Human
{
    [SerializeField] private Transform attackPoint;
    [SerializeField] private Pooling pooling;

    public override void AttackProcessing()
    {
        if (target != null)
        {
            PlaySound(attackSound);
            GameObject bullet = pooling.GetPool();
            bullet.transform.position = attackPoint.position;
            bullet.SetActive(true);
            bullet.GetComponent<Bullet>().SetTarget(this,target, ATK);             
        }
    }
}
